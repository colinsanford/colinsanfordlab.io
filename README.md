## Portfolio

View my portfolio on https://colinsanford.gitlab.io

## Technology

- React JS
- Tailwind
- Vite
- tsParticles
- Popper
