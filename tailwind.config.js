const defaultTheme = require('tailwindcss/defaultTheme');

/**
 * @type {import('tailwindcss').Config}
 */
module.exports = {
    darkMode: 'class',
    content : [
        '*.html',
        './src/**/*.{js,ts,jsx,tsx}'
    ],
    theme   : {
        extend: {
            typography: ({theme}) => ({
                DEFAULT: { // this is for prose class. See https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js
                    css: {
                        color                : 'inherit',
                        '--tw-prose-headings': 'inherit',
                        '--tw-prose-links'   : theme('colors.white'),
                        a                    : {
                            textUnderlineOffset    : '0.25em',
                            textDecorationThickness: '1px'
                        }
                    }
                },
            }),
            colors    : {
                light    : {
                    '200': '#e7e8ee',
                },
                primary  : {
                    '500': '#ff5c00',
                    '700': '#cc4b00'// WCAG compliant for smaller white text
                },
                secondary: {
                    '700': '#456',
                    '900': '#2a3541',
                },
            },
            fontFamily: {
                'sans': ['REM', ...defaultTheme.fontFamily.sans],
            }
        },
    },
    plugins : [
        require('@tailwindcss/typography'),
    ],
};
