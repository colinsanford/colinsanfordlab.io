import ExternalLink from '@/components/ExternalLink';
import {GITLAB_REPO, LINKEDIN, CODEPEN} from '@/config/profiles';
import Particles from '@/components/Particles';

function CallToAction() {
    return (
        <div className='relative bg-secondary-900 py-10 text-center lg:py-24'>
            <div className='width-container relative z-10'>
                <div className='[&_p]:mb-5'>
                    <h2 className='h1 text-center'>Want To See More?</h2>
                    <p>
                        <ExternalLink
                            href={LINKEDIN.url}
                            className='text-link'
                        >
                            Contact me on LinkedIn.
                        </ExternalLink>
                    </p>
                    <p>
                        <ExternalLink
                            href={GITLAB_REPO.url}
                            className='text-link'
                        >
                            View this website's source code on gitlab.
                        </ExternalLink>
                    </p>
                    <p>
                        <ExternalLink
                            href={CODEPEN.url}
                            className='text-link'
                        >
                            View my web experiments on codepen.
                        </ExternalLink>
                    </p>
                </div>
            </div>
            <Particles/>
        </div>
    );
}

export default CallToAction;
