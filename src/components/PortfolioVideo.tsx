type Props = {
    src: string
    caption?: string | React.JSX.Element | null | undefined
}

function PortfolioVideo({src, caption}: Props) {
    return (
        <figure>
            <video
                className='rounded-lg shadow-xl border-slate-600/40 border w-full'
                src={src}
                autoPlay
                muted
                loop
                controls
            ></video>
            {caption ? (
                <figcaption
                    className='mt-4 pb-5 prose max-w-none prose-sm border-b border-b-slate-100/40 md:prose-base md:border-b-0 md:pb-0'
                >{caption}</figcaption>
            ) : null}
        </figure>
    );
}

export default PortfolioVideo;
