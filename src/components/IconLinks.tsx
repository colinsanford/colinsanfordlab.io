import ExternalLink from '@/components/ExternalLink';
import {GITLAB_REPO, LINKEDIN, CODEPEN} from '@/config/profiles';

import {ImCodepen, ImEmbed2, ImLinkedin2} from 'react-icons/im';

type Props = {
    className: string
};

function IconLinks({className}: Props) {
    return (
        <ul className={className}>
            <li>
                <ExternalLink
                    href={GITLAB_REPO.url}
                    aria-label={`${GITLAB_REPO.label}. (Opens in new window)`}
                    title={`${GITLAB_REPO.label}. (Opens in new window)`}
                >
                    <ImEmbed2 size={30} aria-hidden='true'/>
                </ExternalLink>
            </li>
            <li>
                <ExternalLink
                    href={LINKEDIN.url}
                    aria-label={`${LINKEDIN.label}. (Opens in new window)`}
                    title={`${LINKEDIN.label}. (Opens in new window)`}
                >
                    <ImLinkedin2 size={25} aria-hidden='true'/>
                </ExternalLink>
            </li>
            <li>
                <ExternalLink
                    href={CODEPEN.url}
                    aria-label={`${CODEPEN.label}. (Opens in new window)`}
                    title={`${CODEPEN.label}. (Opens in new window)`}
                >
                    <ImCodepen size={25} aria-hidden='true'/>
                </ExternalLink>
            </li>
        </ul>
    );
}

export default IconLinks;
