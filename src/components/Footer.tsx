import IconLinks from '@/components/IconLinks';

function Footer() {
    return (
        <footer>
            <div className='width-container'>
                <div className='flex flex-col gap-5 py-5 text-center md:flex-row md:items-center md:py-10 md:text-left'>
                    <small>
                        <p>Portfolio website development and design by Colin Sanford. Built with React JS and
                            Tailwind.</p>
                    </small>
                    <IconLinks className='flex gap-5 justify-center md:ml-auto md:justify-start'/>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
