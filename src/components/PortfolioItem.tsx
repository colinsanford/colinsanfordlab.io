import {FiChevronRight, FiChevronLeft} from 'react-icons/fi';
import PortfolioMenu from '@/components/PortfolioMenu';

type Props = {
    item: Item
    allItems: Item[]
}

function PortfolioItem({item, allItems}: Props) {
    const nextItem    = allItems[allItems.indexOf(item) + 1] || {id: false};
    const prevItem    = allItems[allItems.indexOf(item) - 1] || {id: false};
    const hasPrevItem = !!prevItem.id;
    const hasNextItem = !!nextItem.id;

    return (
        <article className='last:min-h-[calc(100vh-111px)]'>
            <div
                id={item.id}
                aria-labelledby={`${item.id}-heading`}
                tabIndex={-1}
            ></div>
            <div className='sticky top-0 py-3 lg:py-5 bg-white border-b-slate-600/40 border-b z-10 text-slate-800'>
                <div className='width-container'>
                    <div className='flex items-center'>
                        <h2 className='text-sm sm:text-lg lg:text-3xl font-black'
                            id={`${item.id}-heading`}>{item.title}</h2>
                        <div className='flex ml-auto gap-2 items-center'>
                            <a
                                href={hasPrevItem ? `#${prevItem.id}` : undefined}
                                className={`text-slate-800 hover:text-slate-900 ${!hasPrevItem ? 'opacity-50' : ''}`}
                                aria-label={hasPrevItem ? `Previous section: ${prevItem.title}` : undefined}
                            >
                                <FiChevronLeft
                                    size={40}
                                    aria-hidden='true'
                                />
                            </a>
                            <a
                                href={hasNextItem ? `#${nextItem.id}` : undefined}
                                className={`text-slate-800 hover:text-slate-900 ${!hasNextItem ? 'opacity-50' : ''}`}
                                aria-label={hasNextItem ? `Next section: ${nextItem.title}` : undefined}
                            >
                                <FiChevronRight
                                    size={40}
                                    aria-hidden='true'
                                />
                            </a>
                            <PortfolioMenu
                                items={allItems}
                                activeItem={item}
                                className='ml-5'
                            />
                        </div>
                    </div>
                </div>
            </div>
            <div className='grid gap-5 items-start py-5 width-container md:grid-cols-2 md:gap-10 md:py-16'>
                <div className='flex flex-col gap-10 md:sticky md:top-[calc(81px+40px)]'>
                    {item.media}
                </div>
                <div className='prose md:sticky md:top-[calc(81px+40px)]'>
                    {item.content}
                </div>
            </div>
        </article>
    );
}

export default PortfolioItem;
