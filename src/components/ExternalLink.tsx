type Props = React.AllHTMLAttributes<HTMLAnchorElement> & React.PropsWithChildren;

function ExternalLink({href, children, ...props}: Props) {
    return (
        <a
            href={href}
            target='_blank'
            rel='noopener noreferrer'
            {...props}
        >{children}</a>
    );
}

export default ExternalLink;
