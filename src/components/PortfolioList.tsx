import PortfolioItem from '@/components/PortfolioItem';
import PortfolioVideo from '@/components/PortfolioVideo';
import ExternalLink from '@/components/ExternalLink';

import assetChattersFilterBlog from '@/assets/chatters-blog-filter-2.mp4';
import assetChattersFilterLocation from '@/assets/chatters-location-filter.mp4';
import assetChattersGifPausePlay from '@/assets/chatters-gif-pause-play.mp4';
import assetChattersGalleryDesktop from '@/assets/chatters-gallery-desktop.mp4';
import assetRexallCareers from '@/assets/rexall-careers.mp4';
import assetMyWineCountryCalendar from '@/assets/my-wine-country-calendar.mp4';

function PortfolioList() {
    const items: Item[] = [
        {
            title: 'Chatters - Filter Menus',
            id: 'filter-menus',
            media: (
                <>
                    <PortfolioVideo
                        src={assetChattersFilterBlog}
                        caption={(
                            <p>
                                Filter which updates the href of the apply link based on options selected. <ExternalLink
                                href='https://chatters.ca/blog'>View Site</ExternalLink>
                            </p>
                        )}
                    />
                    <PortfolioVideo
                        src={assetChattersFilterLocation}
                        caption={(
                            <p>
                                Filter which updates Knockout JS observables to filter locations. <ExternalLink
                                href='https://chatters.ca/location'>View Site</ExternalLink>
                            </p>
                        )}
                    />
                </>
            ),
            content: (
                <>
                    <h3>Goal</h3>
                    <p>Create custom filters menus for use across multiple website features.</p>

                    <h3>Notes</h3>
                    <p>Some instances of this are behind a login page, or on an unreleased website feature. There are
                        two examples shown.</p>

                    <h3>Challenges</h3>
                    <ul>
                        <li>The instance on each page has different types of data sources. Some are from
                            Knockout JS observables and others are from database collections.
                        </li>
                        <li>
                            <p>Different behaviours were required when pressing apply for each usage of the filters.</p>
                            <p>Some usages update Knockout JS observables, while another constructs a url + query string
                                based on the selected options.</p>
                            <p>
                                I created a standardized data structure and common base logic to make this more
                                maintainable. The internal data structure was a Knockout JS observable which used an
                                object
                                value with keys for all groups of data. eg: &#123;tags: ['120']&#125;. Initial data
                                passed during initialization determines the type of each value (array or string). Each
                                implementation has method overrides to perform the desired filter actions with this
                                data.
                            </p>
                        </li>
                        <li>Filters may be radio (single option selectable) or checkbox (multi-select). This varies per
                            usage.
                        </li>
                        <li>The user must click “Apply” before the filters are applied.</li>
                        <li>The blog page filters state must persist between pages.</li>
                        <li>The blog page filters must respect any query string parameter it did not add itself.</li>
                    </ul>

                    <h3>Technology</h3>
                    <ul>
                        <li>Magento 2</li>
                        <li>Knockout JS</li>
                        <li>jQuery</li>
                        <li>SCSS</li>
                    </ul>
                </>
            )
        },
        {
            title: 'Chatters - Pause/Play GIF',
            id: 'pause-play-gif',
            media: (
                <PortfolioVideo
                    src={assetChattersGifPausePlay}
                    caption={(
                        <p>
                            Example of GIF pause/play functionality inside the product image gallery. <ExternalLink
                            href='https://chatters.ca/designme-puff-me-volumizing-mousse'>View Site</ExternalLink>
                        </p>
                    )}
                />
            ),
            content: (
                <>
                    <h3>Goal</h3>
                    <p>Add functionality to pause GIF images in various places across the site. For accessibility
                        purposes, the user must be able to pause these.</p>

                    <h3>Notes</h3>
                    <p>I could not find many usages of this functionality on the live site.</p>

                    <h3>Challenges</h3>
                    <ul>
                        <li>
                            <p>This pause/play functionality does not exist natively.</p>
                            <p>
                                After some research and development, I adopted an approach which involves swapping out
                                the image with a canvas element to create a paused version of the GIF. It took a lot of
                                customization and additional functionality to meet all the project requirements.
                            </p>
                        </li>
                        <li>Support normal image tags, and picture tags with different sources.</li>
                        <li>Support many usages in different contexts across the site. From CMS to a Knockout JS
                            powered image gallery.
                        </li>
                        <li>Support GIF images which must be paused when initially shown (in the case of desktop image
                            gallery thumbnails).
                        </li>
                        <li>Support different pause/play button positions to play nicely with other content positioned
                            above the GIF.
                        </li>
                    </ul>

                    <h3>Technology</h3>
                    <ul>
                        <li>Magento 2</li>
                        <li>Knockout JS</li>
                        <li>jQuery</li>
                    </ul>
                </>
            )
        },
        {
            title: 'Chatters - Product Image Gallery',
            id: 'product-image-gallery',
            media: (
                <PortfolioVideo
                    src={assetChattersGalleryDesktop}
                    caption={(
                        <p>
                            Desktop image gallery with sticky navigation. <ExternalLink
                            href='https://chatters.ca/drybar-blowout-with-a-twist-holiday-set-292000036'>View
                            Site</ExternalLink>
                        </p>
                    )}
                />
            ),
            content: (
                <>
                    <h3>Goal</h3>
                    <p>Create a desktop image gallery with sticky vertical navigation. The navigation should have an
                        indicator for the currently visible image.</p>

                    <h3>Challenges</h3>
                    <ul>
                        <li>
                            <p>Creating the current slide indicator in the sticky navigation.</p>
                            <p>
                                There's lots of different ways I could have approach this which seemed messy or
                                complicated. I ended up figuring out an elegant approach by using "IntersectionObserver"
                                on the larger images after they all loaded. Instead of the typical approach of checking
                                the "isIntersecting" property, I kept track of each of the "intersectionRatio"
                                properties in a Knockout JS observable array. I then subscribed to that data and
                                calculated the index of first array element with the largest intersection ratio. This
                                index would be used to update another observable which is used to add the class to the
                                corresponding sticky navigation item. Very simple to understand an no messy measurement
                                logic.
                            </p>
                        </li>
                    </ul>

                    <h3>Technology</h3>
                    <ul>
                        <li>Magento 2</li>
                        <li>Knockout JS</li>
                        <li>jQuery</li>
                        <li>SCSS</li>
                    </ul>
                </>
            )
        },
        {
            title: 'My Wine Country - Accessible Calendar',
            id: 'my-wine-country-calendar',
            media: (
                <PortfolioVideo
                    src={assetMyWineCountryCalendar}
                    caption={(
                        <>
                            <p>
                                My Wine Country Accessible Custom Calendar. Demo shows keyboard only
                                navigation. <ExternalLink
                                href='https://mywinecountry.com/events'>View Site</ExternalLink>
                            </p>
                            <p>
                                A basic version of this calendar with slightly different behaviour is also used on
                                the <ExternalLink
                                href='https://www.mywinecountry.com/on/niagara-wine-festival-discovery-pass.html'>
                                product detail</ExternalLink> pages.
                            </p>
                        </>
                    )}
                />
            ),
            content: (
                <>
                    <h3>Goal</h3>
                    <p>
                        Create a fully custom calendar component that is WCAG 2.0 AA compliant.
                    </p>

                    <h3>Challenges</h3>
                    <ul>
                        <li>
                            <p>Creating the custom interface</p>
                            <p>
                                On this project I explored a number of pre-built options at the time but none of them
                                met the accessibility requirements we had to meet. I explored creating custom
                                functionality I needed to generate data, but didn't feel confident at the time with how
                                reliable the data would be (since that is crucial) and time pressure was building. I
                                ended up compromising and coming up with a more creative way to use an existing date
                                picker library. I created an instance of this date picker outside of the document and
                                used it as a data source for the fully custom Knockout JS interface I created. This
                                wasn't the most ideal solution, but it was reliable and proved I could be creative
                                when solving problems.
                            </p>

                            <p>
                                Around 2 years later on another project (Chatters), a date picker was needed and I
                                solved the data source problem from before. I created some custom logic to use Moment JS
                                to generate date data. This worked very well and was much more performant without
                                sacrificing data reliability. I would have chosen to demo the Chatters date picker, but
                                the feature utilizing it isn't released yet.
                            </p>
                        </li>
                        <li>
                            <p>Keyboard navigation.</p>
                            <p>
                                Having all the dates as focusable makes it really time consuming to navigate by pressing
                                the tab. To resolve this, on desktop only one or two dates are tabbable. The selected
                                date (if applicable) and a highlighted date which the user may choose to select. With
                                arrow keys being used to navigate between the dates, it now becomes easy to access other
                                controls with the tab key.
                            </p>
                            <p>
                                This functionality also needed to account for other custom logic such as disabled past
                                dates, specific dates enabled, and more.
                            </p>
                        </li>
                        <li>Only allowing specific dates to be selected.</li>
                        <li>Show 2 months at a time on desktop, show 1 on mobile.</li>
                        <li>Reset button functionality on category pages.</li>
                        <li>Flexible dates functionality on category pages.</li>
                        <li>
                            Slightly different behaviour and functionality on product detail pages. It closes when
                            a value is selected.
                        </li>
                    </ul>

                    <h3>Technology</h3>
                    <ul>
                        <li>Magento 2</li>
                        <li>Knockout JS</li>
                        <li>jQuery</li>
                        <li>SCSS</li>
                    </ul>
                </>
            )
        },
        {
            title: 'Rexall - Careers Micro-site',
            id: 'rexall-careers-micro-site',
            media: (
                <PortfolioVideo
                    src={assetRexallCareers}
                    caption={(
                        <p>
                            Rexall Careers Micro-site. <ExternalLink href='https://careers.rexall.ca/'>View
                            Site</ExternalLink>
                        </p>
                    )}
                />
            ),
            content: (
                <>
                    <h3>Goal</h3>
                    <p>Create a WCAG 2.0 AA compliant careers micro-site to attract new talent to Rexall.</p>

                    <h3>Notes</h3>
                    <p>This site has been active for a few years with numerous content updates which I have
                        not been involved with. I cannot guarantee that it is still 100% compliant.</p>

                    <h3>Challenges</h3>
                    <ul>
                        <li>
                            <p>IE 11 browser support.</p>
                            <p>Various IE 11 bugs presented themselves and made development of some of the fancier
                                features challenging.</p>
                        </li>
                        <li>
                            <p>Fade effects on some items as you scroll.</p>
                            <p>
                                This needed to be reusable and work in IE 11. It was also made in a time before
                                IntersectionObserver was widely supported, the W3C spec was only a draft, and no
                                polyfill existed.
                                I made a jQuery UI widget with logic based on boxes
                                intersecting which I called "boxIntersection". The final solution was based on my proof
                                of concept on <ExternalLink
                                href='https://codepen.io/colinsanford/pen/aLprOY'>Codepen</ExternalLink>. The
                                final code can most easily be viewed on <ExternalLink
                                href='https://web.archive.org/web/20181019232446/https://www.rexall.ca/static/frontend/Rexall/default/en_US/Rexall_Cms/js/boxIntersection.js'
                            >Wayback Machine</ExternalLink>.
                            </p>
                        </li>
                        <li>
                            <p>Career types content which sticks as you scroll, then fades out when the next section is
                                visible.</p>
                            <p>
                                I utilized my "boxIntersection" functionality, and some additional calculations to
                                determine which section content should be shown. The final code can most easily be
                                viewed on <ExternalLink
                                href='https://web.archive.org/web/20181019232440js_/https://www.rexall.ca/static/frontend/Rexall/default/en_US/Rexall_Cms/js/careers-landing.js?t=1509030641'
                            >Wayback Machine</ExternalLink>.
                            </p>
                        </li>
                        <li>
                            <p>Fancy open/close animation on modals which animates to/from the open button location.</p>
                            <p>
                                It took some effort to get this working and make it performant. Some more advanced usage
                                of css transform ended up working well for this. The final solution was based on my
                                proof of concept on <ExternalLink
                                href='https://codepen.io/colinsanford/pen/eGBpBY'>Codepen</ExternalLink>. The final
                                code can most easily be viewed on <ExternalLink
                                href='https://web.archive.org/web/20181019232446/https://www.rexall.ca/static/frontend/Rexall/default/en_US/Rexall_Cms/js/FancyModal.js'
                            >Wayback Machine</ExternalLink>.
                            </p>
                        </li>
                    </ul>

                    <h3>Technology</h3>
                    <ul>
                        <li>
                            Magento 2. This was later moved to a third party recruiting platform but all frontend code
                            was left intact.
                        </li>
                        <li>jQuery + jQuery UI</li>
                        <li>Less</li>
                    </ul>
                </>
            )
        }
    ];

    return (
        <>
            <div
                id='portfolio'
                tabIndex={-1}
                aria-labelledby={`${items[0].id}-heading`}
            ></div>
            <section>
                {items.map((item, index) => (
                    <PortfolioItem
                        item={item}
                        allItems={items}
                        key={index}
                    />
                ))}
            </section>
        </>
    );
}

export default PortfolioList;
