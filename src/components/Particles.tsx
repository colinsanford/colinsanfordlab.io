import {useId, useCallback, useState} from 'react';

import CONFIG from '@/config/particles.json';

const DEFAULT_CONFIG = CONFIG as ISourceOptions;

import {Particles as TsParticles} from 'react-particles';
import {loadFull} from 'tsparticles';
import type {ISourceOptions, Engine} from 'tsparticles-engine';
import {usePrefersReducedMotion} from '@anatoliygatt/use-prefers-reduced-motion';
import structuredClone from '@ungap/structured-clone';
import {IoIosPause, IoIosPlay} from 'react-icons/io';


type Props = {
    config?: ISourceOptions | undefined
}

function Particles({config = DEFAULT_CONFIG}: Props) {
    const id = useId();

    const [activeConfig, setActiveConfig] = useState<ISourceOptions>(() => structuredClone(config));

    const particlesInit = useCallback(async (engine: Engine) => {
        await loadFull(engine);
    }, []);

    const prefersReducedMotion = usePrefersReducedMotion();

    const [isPlaying, setIsPlaying] = useState(true);

    const [lastIsPlaying, setIsLastPlaying] = useState(() => isPlaying);

    const changePlayState = (shouldPlay: boolean) => {
        if (shouldPlay !== lastIsPlaying) {
            setIsPlaying(shouldPlay);
            setIsLastPlaying(shouldPlay);

            const newConfig = structuredClone(activeConfig);

            newConfig!.particles!.move!.enable = shouldPlay;
            newConfig!.interactivity!.events!.onHover!.enable = shouldPlay;

            setActiveConfig(newConfig);
        }
    }

    const [lastPrefersReducedMotion, setLastPrefersReducedMotion] = useState(() => prefersReducedMotion);

    if (lastPrefersReducedMotion !== prefersReducedMotion) {
        changePlayState(!prefersReducedMotion);
        setLastPrefersReducedMotion(prefersReducedMotion);
    }

    const buttonClickHandler = () => {
        changePlayState(!isPlaying);
    };

    return (
        <>
            <button
                type='button'
                className='absolute top-2 left-2 z-50 p-2 text-white motion-reduce:hidden'
                aria-label={isPlaying ? 'Pause particles animation' : 'Play particles animation'}
                onClick={buttonClickHandler}
            >
                {isPlaying ? (
                    <IoIosPause
                        size={30}
                        aria-hidden='true'
                    />
                ) : (
                    <IoIosPlay
                        size={30}
                        aria-hidden='true'
                    />
                )}
            </button>
            <TsParticles
                id={id}
                className='absolute inset-0 z-0 w-full h-full'
                aria-hidden='true'
                options={activeConfig}
                init={particlesInit}
            />
        </>
    );
}

export default Particles;
