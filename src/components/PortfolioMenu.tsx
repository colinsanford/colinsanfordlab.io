import {useState} from 'react';
import {createPortal} from 'react-dom';

import {FiMenu} from 'react-icons/fi';
import {Popover} from '@headlessui/react';
import {usePopper} from 'react-popper';

type Props = {
    items: Item[]
    activeItem: Item
    className?: string | undefined
}

function PortfolioMenu({items, activeItem, className}: Props) {
    const [referenceElement, setReferenceElement] = useState<HTMLElement| null>(null);
    const [popperElement, setPopperElement]       = useState<HTMLElement| null>(null);
    const {styles, attributes}                    = usePopper(referenceElement, popperElement, {
        placement: 'bottom-end',
        modifiers: [
            {
                name   : 'flip',
                options: {
                    fallbackPlacements: ['top-end'],
                },
            },
            {
                name   : 'offset',
                options: {
                    offset: [0, 20],
                },
            },
        ],
    });

    return (
        <Popover
            className={className}
        >
            <Popover.Button
                ref={setReferenceElement}
                aria-label='Menu'
            >
                <FiMenu
                    size={30}
                    className='translate-y-1'
                />
            </Popover.Button>
            {createPortal(
                (
                    <Popover.Panel
                        unmount={false}
                        className='absolute top-full l-0 z-30 -translate-x-5 translate-y-3 border-slate-600/40 border rounded-md bg-white overflow-hidden'
                        ref={setPopperElement}
                        style={styles.popper}
                        {...attributes.popper}
                        focus
                    >
                        {({close}: {close: any}) => (
                            <>
                                <ul>
                                    {items.map((item) => (
                                        <li
                                            key={item.id}
                                            className={`border-b border-b-slate-600/40 last:border-b-0 ${item === activeItem && 'border-b-0'}`}
                                        >
                                            <a
                                                className={`block px-5 py-3 min-w-[300px] text-slate-800 ${item === activeItem && 'bg-secondary-900 text-white'}`}
                                                href={`#${item.id}`}
                                                onClick={close}
                                            >
                                                {item.title}
                                                {item === activeItem && (
                                                    <span className='sr-only'>(current)</span>
                                                )}
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </>
                        )}
                    </Popover.Panel>
                ),
                document.body
            )}
        </Popover>
    );
}

export default PortfolioMenu;
