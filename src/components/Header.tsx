import IconLinks from '@/components/IconLinks';
import Particles from '@/components/Particles';

import {FiChevronsDown} from 'react-icons/fi';

function Header() {
    return (
        <header
            className='relative flex items-center flex-col min-h-screen w-full bg-gradient-to-b from-secondary-900 to-secondary-800'
        >
            <div
                className='before:bg-gradient-to-b from-primary-500 to-80% to-primary-700 before:inset-0 before:absolute before:-z-1 self-center justify-self-end p-[30px] pt-[100px] relative'
            >
                <div className='relative z-10'>
                    <p className='text-white text-5xl italic font-black md:text-7xl'>Colin <br/> Sanford</p>
                    <p className='lead text-white italic md:text-2xl'>Senior Frontend Web Developer</p>
                </div>
            </div>
            <IconLinks className='relative flex gap-5 mt-5 mb-10 z-10 lg:mb-16'/>

            <div className='relative z-10 my-auto text-center self-center flex-none width-container'>
                <div className='prose lg:prose-2xl max-w-lg mx-auto'>
                    <h1>Portfolio</h1>
                    <p>Welcome to my portfolio website. Scroll to see some of my development work.</p>
                    <div className='not-prose'>
                        <a
                            href='#portfolio'
                            className='relative z-10 inline-block my-6 lg:my-10'
                            aria-label='View Portfolio'
                        >
                            <FiChevronsDown
                                size={60}
                                aria-hidden='true'
                            />
                        </a>
                    </div>
                </div>
            </div>
            <Particles/>
        </header>
    );
}

export default Header;
