type LinkData = {
    label: string,
    url: string
}

export const CODEPEN: LinkData = {
    label: 'Codepen',
    url: 'https://codepen.io/colinsanford'
};

export const LINKEDIN: LinkData = {
    label: 'LinkedIn profile',
    url: 'https://www.linkedin.com/in/colin-sanford-61659a33/'
};

export const GITLAB: LinkData = {
    label: 'Gitlab',
    url: 'https://gitlab.com/colinsanford'
};

export const GITLAB_REPO: LinkData = {
    label: 'View website source',
    url: 'https://gitlab.com/colinsanford/colinsanford.gitlab.io'
};
