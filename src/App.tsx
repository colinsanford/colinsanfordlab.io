import '@/App.css';

import Header from '@/components/Header';
import PortfolioList from '@/components/PortfolioList';
import CallToAction from '@/components/CallToAction';
import Footer from '@/components/Footer';

function App() {
    return (
        <>
            <article>
                <Header/>
                <PortfolioList/>
                <CallToAction/>
                <Footer/>
            </article>
        </>
    );
}

export default App;
