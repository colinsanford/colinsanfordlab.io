declare module "*.mp4" {
    const value: string;
    export default value;
}

declare type Item = {
    title: string,
    id: string,
    media: React.JSX.Element,
    content: React.JSX.Element
}
